// import { createApp } from 'vue'
import App from './App.vue';
// createApp(App).mount('#app')
import './style.css';

// Para Alpine.js
import Alpine from 'alpinejs';

// suggested in the Alpine docs:
// make Alpine on window available for better DX
window.Alpine = Alpine;

Alpine.start();
